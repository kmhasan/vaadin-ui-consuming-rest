package bd.ac.seu.uss.studentvaadinui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentVaadinUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentVaadinUiApplication.class, args);
	}
}
