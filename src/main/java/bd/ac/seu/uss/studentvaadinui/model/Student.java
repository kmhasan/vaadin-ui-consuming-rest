package bd.ac.seu.uss.studentvaadinui.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bson.types.ObjectId;
import org.hibernate.validator.constraints.Email;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@Document
@EqualsAndHashCode(of = "studentId")
public class Student {
    @Id
    @JsonIgnore
    private ObjectId objectId;
    @Indexed(unique = true)
    @NotNull
    private long studentId;
    @NotNull
    private String studentName;
    private List<Address> addressList;
    private List<Phone> phoneList;
    @Indexed(unique = true) @NotNull @Email
    private String emailAddress;

    public boolean addAddress(Address address) {
        if (addressList == null)
            addressList = new ArrayList<>();
        return addressList.add(address);
    }

    public boolean removeAddress(Address address) {
        if (addressList != null)
            return addressList.remove(address);
        else return false;
    }

    public boolean addPhone(Phone phone) {
        if (phoneList == null)
            phoneList = new ArrayList<>();
        return phoneList.add(phone);
    }

    public boolean removePhone(Phone phone) {
        if (phoneList != null)
            return phoneList.remove(phone);
        else return false;
    }
}
