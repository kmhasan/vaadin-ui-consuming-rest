package bd.ac.seu.uss.studentvaadinui.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@EqualsAndHashCode(of = {"countryCode", "areaCode", "number"})
public class Phone {
    private int countryCode;
    private int areaCode;
    @NotNull
    private int number;
}
