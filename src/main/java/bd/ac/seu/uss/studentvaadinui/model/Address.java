package bd.ac.seu.uss.studentvaadinui.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@EqualsAndHashCode(of = {"streetAddress", "city", "country", "postalCode"})
public class Address {
    @NotNull
    private String streetAddress;
    @NotNull
    private String city;
    @NotNull
    private String country;
    @NotNull
    private String postalCode;
}
