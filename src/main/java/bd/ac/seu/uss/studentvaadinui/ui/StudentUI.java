package bd.ac.seu.uss.studentvaadinui.ui;

import bd.ac.seu.uss.studentvaadinui.model.Student;
import bd.ac.seu.uss.studentvaadinui.service.StudentService;
import com.vaadin.annotations.Theme;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToLongConverter;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;

@Theme(ValoTheme.THEME_NAME)
public class StudentUI extends UI {
    @Autowired
    private StudentService studentService;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(new Label("Hello, World"));

        Grid<Student> studentGrid = new Grid<>();
        studentGrid.setWidth("100%");
        studentGrid.addColumn(Student::getStudentId).setCaption("Student ID");
        studentGrid.addColumn(Student::getStudentName).setCaption("Student Name");
        studentGrid.addColumn(Student::getEmailAddress).setCaption("Email Address");
        studentGrid.setItems(studentService.getAllStudents());
        verticalLayout.addComponent(studentGrid);

        FormLayout formLayout = new FormLayout();
        TextField idField = new TextField("ID");
        TextField nameField = new TextField("Name");
        TextField emailField = new TextField("Email Address");
        Button saveButton = new Button("Save");
        saveButton.addStyleName(ValoTheme.BUTTON_LINK);

        formLayout.addComponents(idField, nameField, emailField, saveButton);

        Binder<Student> studentBinder = new Binder<>();
        studentBinder
                .forField(idField)
                .withConverter(new StringToLongConverter("Must be an integer"))
                .bind(Student::getStudentId, Student::setStudentId);
        studentBinder
                .forField(nameField)
                .bind(Student::getStudentName, Student::setStudentName);
        studentBinder
                .forField(emailField)
                .withValidator(new EmailValidator("Must be a valid email address"))
                .bind(Student::getEmailAddress, Student::setEmailAddress);

        studentGrid.addItemClickListener(itemClick -> studentBinder.readBean(itemClick.getItem()));

        saveButton.addClickListener(clickEvent -> {
            Student student = new Student();
            try {
                studentBinder.writeBean(student);
//                studentService.saveStudent(student);
            } catch (ValidationException e) {
                e.printStackTrace();
            }
        });
        verticalLayout.addComponent(formLayout);

        this.setContent(verticalLayout);
    }
}
