package bd.ac.seu.uss.studentvaadinui.service;

import bd.ac.seu.uss.studentvaadinui.model.Student;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

@Service
public class StudentService {
    public List<Student> getAllStudents() {
        List<Student> studentList = null;
        try {
            URL url = new URL("http://172.17.0.44:8080/api/v1/students");

            ObjectMapper objectMapper = new ObjectMapper();
            studentList = objectMapper.readValue(url, new TypeReference<List<Student>>() {});

            studentList.stream().forEach(System.out::println);

            return studentList;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return studentList;
    }
}
